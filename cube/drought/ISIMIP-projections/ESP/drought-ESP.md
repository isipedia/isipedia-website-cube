### Areas affected by - people exposed to drought

Authors: Stefan Lange<sup>1</sup>, Ted Veldkamp<sup>2</sup>, Matthias Mengel<sup>1</sup>, Hannes Müller Schmied<sup>3</sup>, Katja Frieler<sup>1</sup>


Affilitations:
1. Potsdam Institute for Climate Impact Research, Germany
2. Vrije Universiteit Amsterdam, The Netherlands
3. Goethe University Frankfurt, Germany


Published: 15 January 2020
Doi: [to be added]


### Key messages

 
* Spain ranks (ranking-value: land-abs-temp_ESP value: position temperature:2) with regards to absolute changes in land area affected by droughts (expressed as % of Spain’s land area) at 2°C of global warming in comparison to a situation without climate change. For the absolute changes in population exposed to droughts (expressed as % of Spain’s population), Spain ranks (ranking-value: pop-abs-temp_ESP value: position temperature:2).


* At today’s levels of 1°C of global warming the simulated land area affected is already 11500 km<sup>2</sup> larger (2.0% of the land area) than in a world without climate change where the annual area affected by droughts is 600 km<sup>2</sup>  (0.1% of Spain’s land area). The number of people exposed is 0.2 million (0.5% of the population) larger than without climate change where the annual number of people exposed to droughts was 0.0 million (0.1% of Spain’s population).


* At 2°C of global warming the land area affected by droughts would increase by 24500 km<sup>2</sup> (4.9% of the land area) compared to a world without climate change, to 4.8% of the country’s land area. Assuming present-day population patterns, Spain’s population exposed to droughts would increase by 0.9 million, to 1.2% of the population.


* Following the higher-emissions scenario (RCP6.0) which can entail over 3°C of global warming by the end of the century (2081-2100) ([Frieler et al. 2017](https://dx.doi.org/10.5194/gmd-10-4321-2017)) the land area affected by droughts would  increase by 36700 km<sup>2</sup> (7.3% of the land area) and reach 5.8% of the country’s land area. Assuming present-day population patterns the population exposed would reach 1.7% of Spain’s population, and increase by 1.6 million compared to a situation without climate change. 


### How have we got the results?


The assessment provided below is based on our paper “Change in exposure to climate impact events under global warming” submitted to the journal “Earth’s Future”. We have used simulations of 1 different (link: glossary/global-hydrological-models text: global hydrological models) to estimate changes in drought conditions assuming different levels of global mean temperature change. To this end the global hydrological models were forced by climate projections from 3 different global (link: glossary/climate-models text: climate models) following the ISIMIP2b scenario set-up. In the key messages we report the results for the median of the (link: glossary/model-ensemble text: model ensemble) of global hydrological models that ran simulations in (link: glossary/isimip2b text: ISIMIP2b). The median represents the middle of the ensemble, meaning that 50% of the ensemble members provide higher numbers and 50% provide lower numbers.


Among the multiple concepts of “drought”, our study refers to a situation where monthly soil moisture (agricultural drought concept) is extremely low for at least 7 consecutive months. “Extremely low” means dry conditions occurring less than 5 times in 200 years under reference conditions without climate change. We were interested in changes in the land area affected by droughts and the number of people exposed to droughts that we have to expect at different (link: glossary/levels-of-global-warming text: levels of global warming) (1°C, 2°C, and 3°C) or during different future time periods. 


The ISIMIP2b simulations start in 1860 (before human greenhouse gas emissions started to change the climate) and end in 2100. They comprise a long run where weather only varies according to pre-industrial conditions without climate change. In addition, modellers have run two different future scenarios: one “business as usual” scenario (RCP6.0) reaching high levels of warming and one “low emissions scenario” (RCP2.6) with reduced levels of climate change. 


As the “land area affected by drought” varies quite strongly from year to year we decided to average numbers over multiple years: for each year we calculate the deviation of the land area affected by droughts from the average reference level without climate change. These deviations are then averaged over a present-day period (2001-2020), a mid-century period (2041-2060) and an end-of-century period (2081-2100). To calculate the changes at different levels of global mean temperature change we do not average over years belonging to one of these periods but over all years with global mean warming levels close to 1°C, 2°C, and 3°C. 


When calculating the “number of people exposed to droughts” we proceed in a similar way. We assume that only the rural population is exposed while the urban population does not necessarily “feel” any drought. For 1860 to 2005 we account for changes in population patterns ([Klein Goldewijk et al., 2017](https://dx.doi.org/10.5194/essd-9-927-2017)). Afterwards the population data is considered constant -- not as a realistic assumption but an informative “thought experiment”: What would future climate change mean for present day societies? For all time periods, the pure effect of climate change on the “number of people exposed to droughts” is estimated by comparing the number of people exposed under climate change and a specific population pattern to the number of people exposed assuming the same population patterns but no climate change.  


In the following we describe what we have found and how strongly Spain is affected compared to other countries.


### What we have found


###  Land area affected by droughts


(line-plot: land-abs-temp_ESP,land-abs-time_ESP first-temperature: 2 second-scenario: rcp26 second-time: 2041-2060)


Our definition of “drought” is quite strict, such that, without climate change, only 0.1% of Spain’s land area would be affected by droughts each year, on average.


However, at today’s level of 1°C global warming Spain’s annual land area affected by droughts is, on average, already larger and amount to 11500 km<sup>2</sup> (2.3% of the land area). The level of change ranges from 0.1% to 25.9% for the individual combinations of global hydrological models and global climate models. At 2°C of global warming, Spain’s annual land area affected by droughts is projected to increase by 24500 km<sup>2</sup> (i.e. 4.9% of the land area) on average in comparison to a world without climate change. Under these conditions, 4.8% of the land area would be affected by droughts each year, on average. Across the individual combinations of global hydrological models and global climate models this expected level of change ranges from 600 to 129200 km<sup>2</sup>.


Following the higher-emissions scenario (RCP6.0) the land area affected by droughts is expected to increase by 52000 km<sup>2</sup> (10.4% of the land area) towards the end of the century (2081-2100). Following the low emission scenario (RCP2.6) the change would only reach 10800 km<sup>2</sup> (2.2% of the land area). By the middle of the century, changes reach  11400 km<sup>2</sup> under RCP2.6 and 12600 km<sup>2</sup> under RCP6.0. 


 
Spain is the (ranking-value: land-abs-temp_ESP value: position temperature:2) strongest affected by droughts at 2°C of global warming. 



###  Population exposed to droughts


(line-plot: pop-abs-temp_ESP,pop-abs-time_ESP first-temperature: 2 second-scenario: rcp26 second-time: 2041-2060)


Our definition of “drought” is quite strict, such that, without climate change, only 0.0% of Spain’s population would be exposed to droughts each year, on average.

However, at today’s level of 1°C of global warming Spain’s annual population exposed to droughts is, on average, already 0.2 million (i.e. 0.5% of the total population) higher than without climate change and amount to 0.6% of the total population. The level of change ranges from 0.1 % to 25.9% for the individual combinations of global hydrological models and global climate models. At 2°C of global warming, Spain’s annual population exposed to droughts is projected to increase by 0.4 million (i.e. 0.9% of the population) on average in comparison to a world without climate change. Under these conditions, 1.2% of the total population would be affected by droughts, on average. Across the individual combinations of global hydrological models and global climate models this expected level of change ranges from 0.0 up to 2.7 million people.


Following the higher-emissions scenario (RCP6.0) the population exposed to droughts is expected to increase by 0.9 million (2.0% of the total population) towards the end of the century (2081-2100). Following the low emission scenario (RCP2.6) the change would expose 0.1 million people (0.2% of the total population). By mid of the century changes expose 0.2 million people under RCP2.6 and 0.2 million people under RCP6.0.


 
Spain is the (ranking-value: pop-abs-temp_ESP value: position temperature:2) strongest affected by droughts at 2°C of global warming.


###  How is soil moisture calculated?


Soil moisture is defined as the water stored in the soil in liquid or frozen form. It is calculated as the difference between the amount of water coming in as rain or snow, and the amount of water going out, either through evapotranspiration from the surface and vegetation, runoff, or through percolation of water towards the deeper groundwater layers. Global hydrological models (GHMs) include different representations of the (link: glossary/soil-water-column text: soil water column) with different numbers of soil water layer(s) (between 1 and 15 layers), and with different total depth of soil layers (between 1 and 42.1 meter). 
Here, we use (link: glossary/rootzone-soil-moisture text: rootzone soil moisture) estimates, the portion of soil moisture that is within the rooting depth of plants, when directly provided by the GHMs. For GHMs that do not directly provide root zone soil moisture conditions, we approximated this variable by integrating soil moisture across multiple soil water layers in order to reach a depth of ~1 meter. Daily rootzone soil moisture values are finally aggregated into monthly average rootzone soil moisture conditions per grid cell.


### Where are the most relevant gaps in our knowledge?
Models are simplified representations of reality, hence model simulations come with limitations and uncertainties that have to be kept in mind ([Döll et al. 2016](https://dx.doi.org/10.1007/s10712-015-9343-1)). Regarding our drought simulations we mainly see the following issues.


* Representation of direct human influences


The presented drought projections reflect the isolated effect of climate change, while their local manifestations are expected to also be influenced by direct human drivers like changes in land use, land cover, and irrigation. Even in the historical period our simulations only account for water abstractions (with varying sectors among the models like irrigation, industry, domestic, livestock, desalinization) from groundwater and surface waters, reservoir management while other processes such as inter-basin water transfers and a more realistic reservoir management are also expected to be important. Including these drivers into our simulation would greatly improve the simulations ([Veldkamp et al. 2018](https://doi.org/10.1088/1748-9326/aab96f)). That it is not done is mainly an issue of missing knowledge about them.   


* Representation of the CO2 fertilization effect 


Another challenge lies in the representation of how vegetation cover affects evapotranspiration, a relationship that is subject to change under rising temperature and CO2 levels. For example, the effect of CO2 fertilization – the phenomenon through which photosynthesis, hence plant growth, should be enhanced in a CO2-richer atmosphere – is not represented in most global hydrological models as the vegetation components in those models are often simplified. Plants need less water to assimilate the same amount of carbon in a CO2-richer atmosphere. This effect alone would lead to a decrease of evapotranspiration. Yet the more efficient carbon assimilation lets plants grow better, which leads to bigger or more plants. This effect alone would lead to an increase of evapotranspiration. The overall CO2 fertilization effect on evapotranspiration, soil moisture conditions and drought risk is uncertain (Gerten et al., 2014; [Prudhomme et al., 2014](https://dx.doi.org/10.1073/pnas.1222473110); [Döll et al., 2016](https://dx.doi.org/10.1007/s10712-015-9343-1); [Kuzyakov et al., 2019](https://dx.doi.org/10.1016/j.soilbio.2018.10.005)).


* Representation of the soil compartment

The representation of the soil compartment vary with the total depth of soil layers (between 1 and 42.1 m), the number of soil layers (ranging from 1 up to 15) and the conceptualization of soil water processes. In many global hydrological models, we use here root zone soil moisture (related to agricultural needs) as an input for our drought assessment. However, in other models, we have to approximate this variable by integrating soil moisture across multiple water layers. Differences in the representation of the soil column and soil water processes can lead to significant differences in estimates of soil water availability, soil water saturation and drought conditions but this effect could not be investigated for the models used in this assessment.


* Representation of evapotranspiration in general

Much of the differences between the individual hydrological model simulations considered here is assumed to be due to different calculation of potential and actual evapotranspiration.  Whereas some models directly calculates actual evapotranspiration (AET, the amount of water that is transferred through plants, bare soil, open water bodies or from water stored at canopies) directly using water transfer schemes or turbulent fluxes, the majority of the models use the potential evapotranspiration (PET) concept. By using various approaches with different demand on meteorological input variables, the potential water demand of the atmosphere (PET as upper limit) is calculated. The availability of water in the storages (e.g. soil water storage) can reduce the amount of water that is actually evapotranspired to the atmosphere (AET). Unfortunately, currently, we are still not able to decide which of the representations is most appropriate but first assessments of those representations have been done ([Wartenburger et al. 2018](https://doi.org/10.1088/1748-9326/aac4bb)). As a result, estimates of actual evapotranspiration can differ significantly across models despite having used uniform inputs from the global climate models, which may affect estimates of drought conditions much more than for example flooding which mainly depends on the amount of precipitation.


### Disclaimer

Note, that although this report is based on an article published in a peer-reviewed scientific journal, the original publication focused on a global analysis. Therefore, the results presented here for 200+ countries have not been reviewed individually. ISIpedia provides data on country level for convenience, but cannot be held responsible for any issues with the data. Please contact the ISIpedia editorial team (isipedia.editorial.team@pik-potsdam.de) for more information or questions about this report.

### References
Döll, P., Douville, H., Güntner, A., Müller Schmied, H., Wada, Y. (2016) Modelling Freshwater Resources at the Global Scale: Challenges and Prospects. Surveys in Geophysics, 37(2), 195–221. https://dx.doi.org/10.1007/s10712-015-9343-1


Frieler, K. and Lange, S. and Piontek, F. and Reyer, C. P. O. and Schewe, J. and Warszawski, L. and Zhao, F. and Chini, L. and Denvil, S. and Emanuel, K. and Geiger, T. and Halladay, K. and Hurtt, G. and Mengel, M. and Murakami, D. and Ostberg, S. and Popp, A. and Riva, R. and Stevanovic, M. and Suzuki, T. and Volkholz, J. and Burke, E. and Ciais, P. and Ebi, K. and Eddy, T. D. and Elliott, J. and Galbraith, E. and Gosling, S. N. and Hattermann, F. and Hickler, T. and Hinkel, J. and Hof, C. and Huber, V. and J\"agermeyr, J. and Krysanova, V. and Marc\'e, R. and M\"uller Schmied, H. and Mouratiadou, I. and Pierson, D. and Tittensor, D. P. and Vautard, R. and van Vliet, M. and Biber, M. F. and Betts, R. A. and Bodirsky, B. L. and Deryng, D. and Frolking, S. and Jones, C. D. and Lotze, H. K. and Lotze-Campen, H. and Sahajpal, R. and Thonicke, K. and Tian, H. and Yamagata, Y. (2017). Assessing the impacts of 1.5 °C global warming -- simulation protocol of the Inter-Sectoral Impact Model Intercomparison Project (ISIMIP2b). Geoscientific Model Development,10 (12), 4321--4345. https://dx.doi.org/10.5194/gmd-10-4321-2017 


Gerten, D. and Betts, R. and Döll, P. (2014). Cross-chapter box on the active role of vegetation in altering water flows under climate change. In: Field CB, Barros VR, Dokken DJ, Mach KJ, Mastrandrea MD, Bilir TE, Chatterjee M, Ebi KL, Estrada YO, Genova RC, Girma B, Kissel ES, Levy AN, MacCracken S, Mastrandrea PR, White LL (eds) Climate Change 2014: impacts, adaptation, and vulnerability. Part A: global and sectoral aspects. Contribution of working group II to the 5th assessment report of the intergovernmental panel on climate change. Cambridge University Press, Cambridge, pp 157–161


Klein Goldewijk, K., Beusen, A., Doelman, J., Stehfest, E. (2017) Anthropogenic land use estimates for the Holocene – HYDE 3.2. Earth Syst. Sci. Data, 9, 927–953. https://dx.doi.org/10.5194/essd-9-927-2017 


Kuzyakov, Y., Horwath, W. R., Dorodnikov, M. and Blagodatskaya, E. (2019). Review and synthesis of the effects of elevated atmospheric CO2 on soil processes: No changes in pools, but increased fluxes and accelerated cycles, Soil Biology and Biochemistry, 128, 66-78, https://dx.doi.org/10.1016/j.soilbio.2018.10.005


Lange et al. Change in exposure to climate impact events under global warming. Submitted to Earth’s Future.


Prudhomme, C.; Giuntoli, I.; Robinson, E. L.; Clark, D. B.; Arnell, N. W.; Dankers, R.; Fekete, B. M.; Franssen, W.; Gerten, D.; Gosling, S. N.; Hagemann, S.; Hannah, D. M.; Kim, H.; Masaki, Y.; Satoh, Y.; Stacke, T.; Wada, Y. & Wisser, D. (2014). Hydrological droughts in the 21st century, hotspots and uncertainties from a global multimodel ensemble experiment, Proceedings of the National Academy of Sciences, National Academy of Sciences, 111, 3262-3267, https://dx.doi.org/10.1073/pnas.1222473110


Veldkamp, T. I.E., Zhao, F., Ward, P.J., de Moel, H., Aerts, J.C.J.H., Müller Schmied, H., Portmann, F.T., Masaki, Y., Pokhrel, Y., Liu, X., Satoh, Y., Gerten, D. Gosling, S.N., Zaherpour, J., Wada, Y. (2018) Human impact parameterizations in global hydrological models improve estimates of monthly discharges and hydrological extremes: a multimodel validation study. Environ. Res. Lett. 13 055008, https://doi.org/10.1088/1748-9326/aab96f


Wartenburger R., and Sonia I Seneviratne and Martin Hirschi and Jinfeng Chang and Philippe Ciais and Delphine Deryng and Joshua Elliott and Christian Folberth and Simon N Gosling and Lukas Gudmundsson and Alexandra-Jane Henrot and Thomas Hickler and Akihiko Ito and Nikolay Khabarov and Hyungjun Kim and Guoyong Leng and Junguo Liu and Xingcai Liu and Yoshimitsu Masaki and Catherine Morfopoulos and Christoph Müller and Hannes Müller Schmied and Kazuya Nishina and Rene Orth and Yadu Pokhrel and Thomas A M Pugh and Yusuke Satoh and Sibyll Schaphoff and Erwin Schmid and Justin Sheffield and Tobias Stacke and Joerg Steinkamp and Qiuhong Tang and Wim Thiery and Yoshihide Wada and Xuhui Wang and Graham P Weedon and Hong Yang and Tian Zhou. (2018)  Evapotranspiration simulations in ISIMIP2a—Evaluation of spatio-temporal characteristics with a comprehensive ensemble of independent datasets. Environmental Research Letters,13 (7), 075001. https://dx.doi.org/10.1088/1748-9326/aac4bb