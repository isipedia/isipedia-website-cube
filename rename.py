# rename file names
import os, glob

#def listfiles(folder, regex):
#    for root, folders, files in os.walk(folder):
#        for filename in glob.glob(os.path.join(root, regex)):
#            yield filename


#import argparse
#parser = argparse.ArgumentParser()
#parser.add_argument('cube_path')
#parser.add_argument('--indicators', nargs='+', required=True)
#parser.add_argument('--areas', nargs='+')
#o = parser.parse_args()

mapping = {

    "land-area-affected-by-{indicator}-absolute-changes_ISIMIP-projections_versus-temperature-change": "land-abs-temp",
    "land-area-affected-by-{indicator}-relative-changes_ISIMIP-projections_versus-temperature-change": "land-rel-temp",
    "land-area-affected-by-{indicator}_ISIMIP-projections_versus-temperature-change": "land-temp",

    "land-area-affected-by-{indicator}-absolute-changes_ISIMIP-projections_versus-timeslices": "land-abs-time",
    "land-area-affected-by-{indicator}-relative-changes_ISIMIP-projections_versus-timeslices": "land-rel-time",
    "land-area-affected-by-{indicator}_ISIMIP-projections_versus-timeslices": "land-time",

    "land-area-affected-by-{indicator}-absolute-changes_ISIMIP-projections_versus-years": "land-abs-years",
    "land-area-affected-by-{indicator}-relative-changes_ISIMIP-projections_versus-years": "land-rel-years",
    "land-area-affected-by-{indicator}_ISIMIP-projections_versus-years": "land-years",

    "population-exposed-to-{indicator}-absolute-changes_ISIMIP-projections_versus-temperature-change": "pop-abs-temp",
    "population-exposed-to-{indicator}-relative-changes_ISIMIP-projections_versus-temperature-change": "pop-rel-temp",
    "population-exposed-to-{indicator}_ISIMIP-projections_versus-temperature-change": "pop-temp",

    "population-exposed-to-{indicator}-absolute-changes_ISIMIP-projections_versus-timeslices": "pop-abs-time",
    "population-exposed-to-{indicator}-relative-changes_ISIMIP-projections_versus-timeslices": "pop-rel-time",
    "population-exposed-to-{indicator}_ISIMIP-projections_versus-timeslices": "pop-time",

    "population-exposed-to-{indicator}-absolute-changes_ISIMIP-projections_versus-years": "pop-abs-years",
    "population-exposed-to-{indicator}-relative-changes_ISIMIP-projections_versus-years": "pop-rel-years",
    "population-exposed-to-{indicator}_ISIMIP-projections_versus-years": "pop-years",
}

import re

def _replace(f, indicator):
    for k, v in mapping.items():
        restring = k.format(indicator='(.*)')
        m = re.search(restring, f)
        if m:
            v0 = m.group()
            f = f.replace(v0, v)
    return f


#for indicator in o.indicators:
#    d = os.path.join(o.cube_path, indicator)
#
#    for f in listfiles(d, '*.json'):
#        f2 = _replace(f, indicator)
#        if f == f2:
#            continue
#        print(f, f2)
#        os.system('git mv {} {}'.format(f, f2))


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('folder')
o = parser.parse_args()

for f in glob.glob(o.folder+'/*json'):
    f2 = _replace(f, '*')
    if f == f2:
        continue
    print(f, f2)
    os.system('git mv {} {}'.format(f, f2))
